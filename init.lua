-- sc_chicken: MineTest MineClone 2 mod for players who are a bit chicken about damage
-- Copyright (c) 2023 Sacha Chua (sacha@sachachua.com)
--[[

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
	 along with this program. If not, see <https://www.gnu.org/licenses/>.

--]]

local chicken_feet = {
	 description = "Chicken feet",
	 _doc_items_longdesc = "This pair of chicken feet can make you less vulnerable to combat damage.",
	 stack_max = 64,
	 durability = 500,
	 enchantability = 20,
	 points = {
			feet = 10,
			torso = 10,
			legs = 10,
			feet = 10
	 },
	 toughness = 10,
	 inventory_image = "sc_chicken_feet.png",
	 groups = {}
}

minetest.register_craft({
			type = "shaped",
			output = "sc_chicken:chicken_feet",
			recipe = {
				 {"mcl_mobitems:feather", "", "mcl_mobitems:feather"},
				 {"mcl_mobitems:feather", "", "mcl_mobitems:feather"},
			}
})

if minetest.get_modpath("mcl_armor") then
	 local data = {}
	 local function become_chicken(player, itemstack)
			local current = player:get_properties()
			data[player] = {
				 fall_damage_add_percent = current.fall_damage_add_percent
			}
			player:set_properties({hp = current.hp_max, fall_damage_add_percent = 0})
			minetest.chat_send_player(player:get_player_name(), "You are now a chicken.")
	 end
	 local function become_normal(player)	
			player:set_properties(data[player])
			data[player] = nil
			minetest.chat_send_player(player:get_player_name(), "You are no longer a chicken. Yay you!")
	 end
	 minetest.register_on_player_hpchange(function(player, hp_change, reason)
				 if hp_change < 0 and data[player] then
						return 0, true
				 else
						return hp_change
				 end
	 end, true)
	 chicken_feet._on_equip = become_chicken
	 chicken_feet._on_unequip = become_normal
	 chicken_feet.groups.armor = 1
	 chicken_feet.groups.armor_feet = 1
	 chicken_feet.groups.combat_armor = 1
	 chicken_feet.groups.immortal = 1
	 chicken_feet.groups.enchantability = 1
	 chicken_feet.mcl_armor_uses = 1000
	 chicken_feet._mcl_armor_element = "feet"
	 chicken_feet._mcl_armor_texture = "sc_chicken_feet.png"
	 chicken_feet.craft_material = "mcl_mobitems:feather"
	 minetest.register_on_joinplayer(function(player)
				 if player:get_inventory():get_stack("armor", 5):get_name() == "sc_chicken:chicken_feet" then
						become_chicken(player)
				 end
	end)

end

minetest.register_craftitem("sc_chicken:chicken_feet", chicken_feet)
